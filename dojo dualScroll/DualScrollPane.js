﻿define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_Contained",
	"dijit/_Container",
	"dojo/dom-construct",
	"dijit/registry",
	"dojox/mobile/ScrollableView",
	"dojo/on",
	"dojo/_base/lang",
	"dojo/topic",
	"dojo/_base/array"
],function(declare,WidgetBase,Contained,Container,domConst,registry,ScrollablePane,on,lang,topic,array){
	
	
	var DualScrollPane = declare("DualScrollPane",[WidgetBase,Contained,Container],{
		//是否上锁
		_lock:false,
		baseScroll:"dualScroll",
		buildRendering:function(){
			this.inherited(arguments);
			this.centerPane = domConst.create("div",{className:"centerView"},this.containerNode);
			this.leftPane = domConst.create("div",{className:"leftPane"},this.centerPane);
			this.leftScroll = new ScrollablePane({signal:"_l",height:"auto"});
			this.leftScroll.onTouchMove = DualScrollPane._onTouchMove;
			this.leftScroll.slideTo = DualScrollPane._slideTo;
			var temp = domConst.create("div",{innerHTML:"asdaaaaa<br>aa<br>aa<br>aa<br>aa<br>11<br>22<br>33<br>44<br>55<br>66<br>77<br>88<br>99<br>10<br>13<br>15<br>16<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>"});
			this.leftScroll.domNode.appendChild(temp)
			this.leftPane.appendChild(this.leftScroll.domNode);
			this.lockPane = domConst.create("div",{className:"midLock"},this.centerPane);
			var lockRegion = domConst.create("div",{style:"margin:auto;height:30px;width:30px;backgroundColor:yellow;top:45%;position:relative"},this.lockPane);
			this.lockImg = domConst.create("img",{width:"30px",height:"30px",src:"demo/themes/lock-unlocked.png"},lockRegion);
			this.rightPane = domConst.create("div",{className:"rightPane"},this.centerPane);
			this.rightScroll = new ScrollablePane({signal:"_r",height:"auto"});
			this.rightScroll.onTouchMove = DualScrollPane._onTouchMove;
			this.rightScroll.slideTo = DualScrollPane._slideTo;
			var temp = domConst.create("div",{innerHTML:"11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>11aaa<br>a2a<br>a3a<br>a5a<br>a7a<br>191<br>262<br>3f3<br>4a4<br>5f5<br>6a6<br>7c7<br>8g8<br>9q9<br>130<br>163<br>1g5<br>1bb6<br>"});
			this.rightScroll.domNode.appendChild(temp);
			this.rightPane.appendChild(this.rightScroll.domNode);
		},
	
		postCreate:function(){
			on(this.lockPane,"click",lang.hitch(this,this.onLock));
			topic.subscribe("/demo/DualScroll",lang.hitch(this,this.dualScroll));
			topic.subscribe("/demo/AnimDualScroll",lang.hitch(this,this.dualSlide));
		},
	
		//2个同时slide
		dualSlide:function(signal,from,to,duration,easing,node,temp){
			if(this._lock){
				this.leftScroll._runSlideAnimation(from,to,duration,easing,this.leftScroll.containerNode,temp);
				this.rightScroll._runSlideAnimation(from,to,duration,easing,this.rightScroll.containerNode,temp);
			}
			else{
				if(signal == "_l"){
					this.leftScroll._runSlideAnimation(from,to,duration,easing,node,temp);
				}
				else{
					this.rightScroll._runSlideAnimation(from,to,duration,easing,node,temp)
				}
			}
		},
	
		
		//2个同时移动
		dualScroll:function(signal,to){
			//同时移动
			if(this._lock){
				this.leftScroll.scrollTo(to);
				this.rightScroll.scrollTo(to);
			}
			else{
				if(signal == "_l"){
					this.leftScroll.scrollTo(to);
				}
				else{
					this.rightScroll.scrollTo(to)
				}
			}
		},
		
		addLChild:function(){
		
		},
		
		addRChild:function(){
		
		},
		
		//改变锁的图标
		_changeLockState:function(){
			
		},
	
		//改变锁的状态
		onLock:function(){
		//	console.log("lock")
			if(this._lock){
				this._lock = false;
				this._changeLockState(this._lock);
				this.lockImg.src = "demo/themes/lock-unlocked.png"
			}
			else{
				this._lock = true;
				this._changeLockState(this._lock);
				this.lockImg.src = "demo/themes/lock.png"
			}
		},
		
		resize:function(){
			array.forEach(this.getChildren(), function(child){
				if(child.resize){ child.resize(); }
			});
		},
		
		startup:function(){
			if(this._start) return;
			this.inherited(arguments);
			this.resize();
		}
	
	
	
	});
	
	
	DualScrollPane._onTouchMove = function(e){
			// summary:
			//		User-defined function to handle touchMove events.
			if(this._locked){ return; }
			var x = e.touches ? e.touches[0].pageX : e.clientX;
			var y = e.touches ? e.touches[0].pageY : e.clientY;
			var dx = x - this.touchStartX;
			var dy = y - this.touchStartY;
			var to = {x:this.startPos.x + dx, y:this.startPos.y + dy};
			var dim = this._dim;

			dx = Math.abs(dx);
			dy = Math.abs(dy);
			if(this._time.length == 1){ // the first TouchMove after TouchStart
				if(this.dirLock){
					if(this._v && !this._h && dx >= this.threshold && dx >= dy ||
						(this._h || this._f) && !this._v && dy >= this.threshold && dy >= dx){
						this._locked = true;
						return;
					}
				}
				if(this._v && Math.abs(dy) < this.threshold ||
					(this._h || this._f) && Math.abs(dx) < this.threshold){
					return;
				}
				this.addCover();
				this.showScrollBar();
			}

			var weight = this.weight;
			if(this._v && this.constraint){
				if(to.y > 0){ // content is below the screen area
					to.y = Math.round(to.y * weight);
				}else if(to.y < -dim.o.h){ // content is above the screen area
					if(dim.c.h < dim.d.h){ // content is shorter than display
						to.y = Math.round(to.y * weight);
					}else{
						to.y = -dim.o.h - Math.round((-dim.o.h - to.y) * weight);
					}
				}
			}
			if((this._h || this._f) && this.constraint){
				if(to.x > 0){
					to.x = Math.round(to.x * weight);
				}else if(to.x < -dim.o.w){
					if(dim.c.w < dim.d.w){
						to.x = Math.round(to.x * weight);
					}else{
						to.x = -dim.o.w - Math.round((-dim.o.w - to.x) * weight);
					}
				}
			}
			topic.publish("/demo/DualScroll",this.signal,to);
		//	this.scrollTo(to);

			var max = 10;
			var n = this._time.length; // # of samples
			if(n >= 2){
				// Check the direction of the finger move.
				// If the direction has been changed, discard the old data.
				var d0, d1;
				if(this._v && !this._h){
					d0 = this._posY[n - 1] - this._posY[n - 2];
					d1 = y - this._posY[n - 1];
				}else if(!this._v && this._h){
					d0 = this._posX[n - 1] - this._posX[n - 2];
					d1 = x - this._posX[n - 1];
				}
				if(d0 * d1 < 0){ // direction changed
					// leave only the latest data
					this._time = [this._time[n - 1]];
					this._posX = [this._posX[n - 1]];
					this._posY = [this._posY[n - 1]];
					n = 1;
				}
			}
			if(n == max){
				this._time.shift();
				this._posX.shift();
				this._posY.shift();
			}
			this._time.push((new Date()).getTime() - this.startTime);
			this._posX.push(x);
			this._posY.push(y);
		};
	
	DualScrollPane._slideTo = function(to, /*Number*/duration, /*String*/easing){
			// summary:
			//		Scrolls to the given position with the slide animation.
			// to:
			//		The scroll destination position. An object with x and/or y.
			//		ex. {x:0, y:-5}, {y:-29}, etc.
			// duration:
			//		Duration of scrolling in seconds. (ex. 0.3)
			// easing:
			//		The name of easing effect which webkit supports.
			//		"ease", "linear", "ease-in", "ease-out", etc.
			console.log("slideTo!!!")
			topic.publish("/demo/AnimDualScroll",this.signal,this.getPos(),to,duration,easing,this.containerNode,2);
		//	this._runSlideAnimation(this.getPos(), to, duration, easing, this.containerNode, 2);
		//	this.slideScrollBarTo(to, duration, easing);
		};
	
	return DualScrollPane;
})