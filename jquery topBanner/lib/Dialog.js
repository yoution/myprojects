
//Dialog  对话框 
// 
//
function Dialog( draggable ) {

	this.showed = false;    //对话框的显示影藏状态
	this.element = null; 
	this.selected = null;//选中的元素
	this.storeSelected = [];  //最后应用规则的元素
	this.rule = '';		//最终运用的规则
	this._isMedia = 0;  //如果选中的元素为简单元素， 1： 图片, 2: object | embed

	this.init();
	//增加拖动功能
	if( draggable ) {
		new Draggable(this.element);
	}
}


Dialog.prototype = {	
	init: function() {
		var elemnetTemplate,

		elemnetTemplate =   '<div id="dialog-wrapper">'	+
								'<div id="dialog-head">hello</div>' +
						 		'<textarea id="dialog-content"></textarea>' +
						 		'<div id="dialog-buttons">'+
						 			'<button id="dialog-preview-button"><span>预览</span></button>' +
						 			'<button id="dialog-add-button"><span>添加</span></button>' +
						 			'<button id="dialog-cancel-button"><span>取消</span></button>' +
						 		'</div>' +
							'</div>';
		//渲染模板					
		this.element = $(elemnetTemplate).appendTo('body');	

		//标题栏
		this.header = $('#dialog-head', this.element);
		//文本内容框
		this.context = $('#dialog-content', this.element);
		//预览, 添加，取消 按钮
		this.previewButton = $('#dialog-preview-button', this.element);
		this.addButton= $('#dialog-add-button', this.element);
		this.cancelButton= $('#dialog-cancel-button', this.element);

		//初始化绑定事件
		this._addHandlers();
	},

	show: function(x, y) {

		if ( this.showed ) {
			return;
		}
		this.showed = true;
		this.element.css({
			left: x,
			top: y,
			display: 'block'
		})
		
		//取消选择操作	
		this.stopSelect();
		//获取rule
//		this.getRule();
	},

	//开始选择要被影藏的区域
	startSelect: function() {
		var preSelected,
			that = this;

		//如果选中增加黄色背景,移出之前选中的元素的背景色
		$('body').bind('mouseover.select', function( evt ){
			var selected = evt.target;
			//禁止选中body和弹出框
//			if( selected.tagName == 'BODY' || /^dialog/.test(selected.id) || /^dialog/.test(selected.parentNode.id)) {
			if( selected.tagName == 'BODY' || this.showed ) {
				return;
			}
				
			if ( preSelected && preSelected != selected ) {
				$(preSelected).removeClass('ad-selecting');

			}
			//暂存之前选中的元素
			preSelected = selected;

			$(selected).addClass('ad-selecting');

			//存入本地数据库， 用于清除操作
			that.selected = selected;
			//.trigger();
			//console.log(evt.target.id);
			// $(evt.target).bind('mouseenter.select', function(evt) {
			// 	$(this).addClass('ad-selecting');
			// 	$(this).trigger();
			// 	return false;
			// }).bind('mouseleave.select', function(evt) {
			// 	$(this).removeClass('ad-selecting');
			// 	return false;
			// });
		});
	},

	//停止选择操作
	stopSelect: function() {
		$('body').unbind('.select');
	//	$('.ad-selecting').die('click');
	},

	//设置标题内容
	setHead: function( value ) {
		this.header.HTML( value );
	},


	hide: function() {
		if ( !this.showed ) {
			return;
		}

		//单个元素清理操作 ? 没操作的情况下点了取消
		$(this.selected).removeClass('ad-selecting');
		this.showed = false;
		//清除储存仓库
		this.storeSelected.removeClass('ad-selected');
		this.storeSelected = [];
		this.element.css({display: 'none'});
		this.clearSelected();
	},

	//获取规则地址
	getHostName: function() {
		var urlInfo = document.location,
			thost = urlInfo.href,
			rule = '',	//由www||% + host + @@ +type......组成
			hostName = '',
			hostSuffix, //host后缀
		//	uselect, 	//选中的元素
			
			parts,
			i = 1,
			MID = '@@',	//规则占位符
			MTYPE = '$$', // 图片，js等占位符
			isType = false, //是否是图片,flash等
	//		regH = /\/\/(www)?.([\w.]+(?:com|cn|net))\/[\d\D]+(?:(\/\d+\/?)|(html|asp|shtml|htm|php)|(\/\w+\/?))$/
			regH = /\/\/(www.)?([\w.]+(?:com|cn|net))\/[\d\D]+(?:(html|asp|shtml|htm|php)|(\/\w+\/?))$/;

		return urlInfo.hostname + MID; 


		parts = regH.exec(thost);
		//如果是已www开头，则用www开头，否则用%代替
		if ( !parts[1] && parts[2] ) {
			hostName += ['%',[2]].join('.');	
		}
		else {
			hostName += [parts[1],parts[2]].join('.');
		}

		if (parts[3]) {
			hostName += '*.' + parts[3];
		}


		return hostName + MID;

		//selected = this.selecteds;
		// if ( selecteds.length == 1 &&  )


	},


	//获取规则,优先级 class id tag 
	_getRule: function(elem) {
		var tclass,
			tid, 
			ttag;


		tclass = elem.className && '.'+elem.className.split(' ').join('.');
		tid = elem.id && '#'+elem.id;
		ttag = elem.tagName.toLowerCase();

		return tclass || tid || ttag;
	},



	//根据选中的元素提取规则
	getSelectedRule: function() {
		var selected = this.selected,
			regType = /(img)|ebmed|object|iframe/i,
			tParts,
			rule,
			lRule = '',
			rRule = '';


		tParts = regType.exec(selected.nodeName.toLowerCase()); 

		//如果是图片等特征元素
		if ( tParts ) {
			if ( tParts && tParts[0] && tParts[1] ) {
				this._isMedia = 1;
				rule = /\/\/([\w\W]*)$/.exec(selected.src);
				return '%'+ rule[1] + '$$iamge';
			}
			else {
				this._isMedia = 2;
				return tParts[0]; 
			}

		}
		

		$(this.selected).removeClass('ad-selecting');
		//TODO 暂时规则取选中元素和父元素组成，优先级 class id tag 
		rRule = this._getRule( selected );
		$(this.selected).addClass('ad-selecting');

		if( selected.parentNode ) {
			lRule = this._getRule( selected.parentNode );
		}

		return lRule + ' ' + rRule ;
	},



	showRule: function(value) {
		this.context.val(value || '');
	},



	//预览选中的元素
	showSelected: function(){

		this.storeSelected.addClass('ad-selected');

	},

	//影藏选择的元素
	hideAll: function() {
		this.storeSelected.hide();
		this.hide();
		this.send();
	},





	//取消选择的元素
	clearSelected: function() {

		var elems = this.storeSelected,
			len = this.storeSelected.length; 

		for (var i = 0; i < len ; i++) {
			elems.removeClass('ad-selected');
		}	

		this._isMedia = 0;
	},

	//销毁对象
	destroy: function() { 

	},

	isShow: function() {
		return this.showed;
	},

	send: function() {

	},



	//绑定事件对象
	_addHandlers: function() {
		this.addButton.bind('click.dialog', $.proxy(this.hideAll, this));
		this.previewButton.bind('click.dialog', $.proxy(this.showSelected, this));
		this.cancelButton.bind('click.dialog', $.proxy(this.hide, this));
		//在选中的元素上点击弹出对话框
		$('.ad-selecting').live('click', $.proxy(this._showActionBridge, this));
		$(document).bind('click', $.proxy(this._mediaProxyAction, this))
	//	$('body').bind('click', $.proxy(this._showActionBridge,this));
	},

	//flash 代理
	_mediaProxyAction: function() {
		if( this.selected && (this.selected.nodeName.toLowerCase() == 'object' || this.selected.nodeName.toLowerCase() == 'embed' )) {
			this._showActionBridge();
		}
		return;
	},


	_showActionBridge: function( evt ) {

		var host = '';
	//	evt.stopPropagation();	
	//	evt.stopImmediatePropagation();

	//	if ( !this.selected ) {
	//		return;
	//	}

		//文本框位置设死
		var pos = {x: 100, y: 100 };

		this.show(pos.x, pos.y);

		//获取规则和host信息
		
		this.rule = this.getSelectedRule();
		//如果是图片，则不取host地址
		if ( this._isMedia !== 1 ) {
			host = this.getHostName();
		}
		this.storeSelected = $( this._isMedia && this.selected || this.rule);

		rules = host + this.rule;
		//显示文本框内容
		this.showRule(rules);
		return false;
	},

	//获取位置
	_getPos: function(x, y) {

	} 


}
