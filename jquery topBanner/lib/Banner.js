
//Banner 顶部banner，动画移出
//
//
function Banner( dialog ) {

	this._showed = false;
	this.element = null; 
	this.dialog = dialog

	this.init();
}


Banner.prototype = {	
	init: function() {
		var elemnetTemplate,

		elemnetTemplate =   '<div id="banner-wrapper">'	+
								'<div id="banner-container"></div>' +
								'<button id="banner-start-button"><span>开始<span></button>' +
							'</div>';
		//渲染模板					
		this.element = $(elemnetTemplate).appendTo('body');	

		this.startButton= $('#banner-start-button', this.element);
		this.content = $('#banner-container', this.element);
		//初始化绑定事件
		this._addHandlers();
	},

	setHead: function( value ) {

	}, 

	show: function() {
		if( this._showed ) {
			return;
		}
		this._showed = true;

		this.element.animate({
			top: '0'
		},'slow');
	},

	hide: function() {
		if( !this._showed ) {
			return;
		}
		this._showed = false;
		this.element.animate({
			top: '-32'
		},'slow');
	},

	isShow: function() {
		return this._showed;
	},

	//绑定事件
	_addHandlers: function() {
		this.startButton.bind('click', $.proxy(this.dialog.startSelect, this.dialog));
	}
}
