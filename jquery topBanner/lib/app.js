$(function(){

	var banner;

 		banner = new Banner( new Dialog( true ) );

	//添加ctrl + 右键响应事件 
	function addKeyHandler( evt ) {
		var menuAction;   //暂存右键菜单	
		
		if ( evt.ctrlKey && evt.button === 2 && !banner.isShow() ) {

			menuAction = document.oncontentmenu;
			document.oncontextmenu = function(){ return false; }
			//显示顶部banner
			banner.show();
		} else if ( banner.isShow() ) {
			//影藏顶部banner
			banner.hide();
		}
		//还原鼠标右键菜单
		setTimeout(function(){
			document.oncontextmenu = menuAction;	
		},1000);	
	}
	//test
//	var a = new Dialog(true);
//	b = 2	
// 	$(document).bind('click', function() {
// 		// if (b != 2) {
// 		// 	return
// 		// }
// 		a.startSelect();
// //		a._showActionBridge();
// 	b++
// 	})

	$(document).bind('mousedown', addKeyHandler );

})