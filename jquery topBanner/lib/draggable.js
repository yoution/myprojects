//拖动模块
function Draggable( elem ) {
	this.element = $(elem); // element为jquery对象

	//移动的距离
	this.moved = {x: 0, y: 0} 

	//添加拖动响应事件
	this._addDragHandler();

}


Draggable.prototype = {


	//获取上次的位置
	getPos: function() {
		var x = this.element.css('left'),
			y = this.element.css('top');

		pos = { x: parseInt(x), y: parseInt(y) };

		return pos;
	},



	onDraging: function( evt ) {
		var pos;


		pos = this.getPos();
		this.moved = {x: evt.screenX - this.startPos.x, y: evt.screenY - this.startPos.y}

		this.element.css({
			left: pos.x + this.moved.x ,
			top: pos.y + this.moved.y
		})
		this.startPos = { x: evt.screenX, y: evt.screenY }
	},


	stopDrag: function( evt ) {
		this.element.unbind('.drag');
	},

	startDrag: function( evt ) {

		this.startPos = { x: evt.screenX, y: evt.screenY };


		this.element.bind('mousemove.drag', $.proxy(this.onDraging, this));
		this.element.bind('mouseup.drag', $.proxy(this.stopDrag, this));
	},

	_addDragHandler: function() {
		this.element.bind('mousedown', $.proxy(this.startDrag, this));
	}
}