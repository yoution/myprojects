//Z 仿照jquery写的，根据项目需要封装的接口

(function(){

	//	
	//	
	//	
	//	

	window.Z = function (sel) {
		return new _Z(sel);	
	};



	function _Z (sel) {
		var elements = [],
			reg = /^(?:(#)?(\.)?([\S]*)$)/i,
			regH = /</i,
			seg,
			all,
			len,
			parts;

		//选择的元素
		this.elements = [];
		//暂存选择符
		this.sel = sel;

		//如果是html片段
		if (regH.test(sel)) {
			seg = document.createElement('div');
			seg.innerHTML = sel;
			elements = [seg.firstChild];
		} else {
		    try{
		    	elements = document.querySelectorAll(sel);
		    } catch(err) {
		    	parts = reg.exec(sel);
		    	if( parts[1] )
		    	{
		    		//统一格式
		    		elements = [document.getElementById(parts[3])];
		    	} 
		    	else if ( parts[2] ) {
		    		try{
		    			elements = document.getElementsByClassName(parts[3]);
		    		} catch (err){
		    			//ie的 getClass
		    			elements = this._getsByClass(parts[3]);
		    		}
		    	} else {
		    		elements = document.getElementsByTagName(parts[3]);
		    	}
		    }
		}	
		this.elements = elements;
	}



	_Z.prototype = {

		
		each: function (fn) {

			for (var i = 0, len = this.elements.length; i < len; i++ ) {
				fn.call(this, this.elements[i]);
			}

			return this;
		},
		//获取样式或设置样式，一次只能设置一个
		css: function(name, value) {
			var tName,
				elem;

			function getStyle (elem, name) {
				if (elem.style && elem.style[name]) {
					return elem.style[name];
				} else if (elem.currentStyle) {
					return elem.currentStyle[name];
				} else if (document.defaultView && document.defaultView.getComputedStyle) {
					name = name.replace(/(A-Z)/g, '-$1');
					name = name.toLowerCase();
					var s = document.defaultView.getComputedStyle(elem);
					return s && s.getPropertyValue(name);
				} else {
					return null;
				}
			}

			function setStyle(elem, name, value) {
				if(elem.style){
					elem.style[name] = value;
				}else {
					elem.currentStyle[name] = value;
				}
			}

			if (!value) {
				return getStyle(this.elements[0], name)
			}

			this.each( function(elem){
				if (value) {
					setStyle(elem, name, value);
				}
			})

			return this;
		},

		//设置class,一次只能一个
		addClass: function (value) {
			var reg = new RegExp(value,'i');

			this.each(function (elem) {
				if (reg.test(elem.className)) {
					return;	
				}
				if (elem.className) {
					elem.className += ' ' +value;
				}	
				else {
					elem.className = value;
				}
			});
			return this;
		},

		removeClass: function (value) {
			var reg = new RegExp(value,'i'),
				name;

			this.each(function (elem) {
				if ( reg.test( elem.className )) {
					name = (' '+elem.className).replace(' '+reg.source, '');
					elem.className = name.substring(1);
				}
			});

			return this;
		},



		//绑定事件,同一类型的只能绑定一个，如click只能绑定一个click
		bind: function(type, fn) {
			var that = this, 
				add = function(elem) {

					if (window.addEventListener) {
						elem.addEventListener(type, handler, false);
					} else if (window.attachEvent) {
						elem.attachEvent('on' + type, handler);
					}
					elem = null;
				},

				handler = function( evt ){
					var evt = that._fixEvent(evt);
					handler[type]( evt );
				}
			//执行函数
			handler[type] = fn;
			//将函数放入缓存	
			Z.cache[this.sel] = handler;


			this.each(function(elem){
				add(elem);
			});

			return this;
		},

		//解除绑定
		unbind: function(type) {
			var handler = Z.cache[this.sel],

			
				remove = function(elem, type) {

				if (window.addEventListener) {
					elem.removeEventListener(type, handler, false);
				} else if (window.datachEvent) {
					elem.datachEvent('on' + type, handler);
				}
				elem = null;
			}

			delete handler[type];

			this.each(function (elem) {
				remove(elem, type);		
			})


		},
	
		//class 的live
		live: function(type, fn) {
			var sel = this.sel.replace('.', '');

			    liveHandler = function(evt) {
			    	if ( evt.target.className == sel ) {
			    		fn();
			    	}
				};

			Z('body').bind( type, liveHandler );

		},


		die: function( type ) {
			Z('body').unbind('click');
		},

		//动画操作
		animate: function(name, value) {
			var cur = parseInt(this.css(name));
				value = parseInt(value),
				minus = value - cur;
			this.each(function(elem){
				for ( var i = 0; i <= 100; i+= 5 ) {
					(function(){
						var pos = i;						
						setTimeout(function(){
							elem.style[name] = cur + (pos / 100) * minus+ 'px';
						//调速度
						}, (pos + 1) * 3);
					})()
				}
			})
		},

		//元素相对页面的偏移,无单位
		offset: function() {
			var elem = this.elements[0];

			function pageX (elem) {
				return elem.offsetParent ? 
					elem.offsetLeft + pageX(elem.offsetParent):
					elem.offsetLeft;	
			}
			function pageY (elem) {
				return elem.offsetParent ?
					elem.offsetTop + pageY(elem.offsetParent):
					elem.offsetTop;
			}

			return {x: pageX(elem), y: pageY(elem)};
		},

		//元素大小, width,height
		box: function() {
		//	return {width: this.css('width'), height: this.css('height')};

			return {width: this.elements[0].clientWidth,height: this.elements[0].clientHeight};
		},

		//设置元素的位置或者获取元素的位置
		position: function() {
			var x = arguments[0], y = arguments[1];
			if( !arguments.length ) {
				return {left: this.css('left'), top: this.css('top')};
	 		} else {
	 			this.css('left', x).css('top', y);
	 		}
	 		return this;
		},

		_fixEvent: function (e) {
			e = e || window.event;
			e.target =	e.target || e.srcElement;

			e.stopPropagation = function() {
				try{
					this.stopPropgation()
				} catch (err) {
					this.cancelBubble = true;
				}
			};

			e.preventDefault = function() {
				try{
					this.preventDefault();
				} catch (err) {
					this.returnValue = false;
				}
			} 

			return e;
		},

		//ie的getclass选择器
		_getsByClass: function (cName) {
			var elems = [],
				allElems,
				len,
				reg = new RegExp('(^|\\s)' + cName + '(\\s|$)');
				
			allElems = document.getElementsByTagName('*');
			len = allElems.length;

			for( var i = 0; i < len; i++ ) {
				if (reg.test(allElems[i].className)) {
					elems.push(allElems[i]);
				}
			}
			return elems;
		}


	};

	//工具函数，切换上下文
	Z.proxy = function(fn, context,arg) {
		return function() {
			fn.call(context,arg);	
		}	
	};

	//事件cache，{'select', handler().([type],fn)
	// 						handler().([type],fn)
	//			}
	Z.cache = {};
	
	/*Z.E = {
		add: 

		remove: 
	}*/


})()