﻿define([
	"dojo/_base/declare",
	"demo/_StoreCardexMixin",
	"dojo/_base/lang",
	"dijit/_WidgetBase",
	"dijit/_Container",
	"dijit/_contained",
	"dojo/dom-geometry",
	"dojo/_base/array"
],function(declare,StoreCardexMixin,lang,WidgetBase,Container, Contained,domGeom,array){
	var CardexContainer = declare("demo.CardexContainer",[WidgetBase,Container,Contained,StoreCardexMixin],{
		//储存显示的category
		_storeCategorys:null,
		
		baseClass:"dijitCardexContainer",
		//已近初始化的item，用于平移距离，使各个iten靠的更近
		_initialedChildren: 0,
		//item之间的间隔，默认30px
		childOffset:30,
		
		constructor:function(args){
			lang.mixin(this,args);
			this._storeCategorys = {};
		},
		
		startup:function(){
			var _this = this;
			array.forEach(this.getChildren(),function(child,idx){
				if(child.startup){
					child.startup();
					//暂存category
					if(!_this._storeCategorys[child.category]){
						_this._storeCategorys[child.category]=1;
					}
					else{
						_this._storeCategorys[child.category]++;
					}
					//每个item的向上偏移
					if(!_this.offset){
						_this.offset = parseFloat(child.domNode.clientHeight) - _this.childOffset;
					}
				}
			});
			this.resize();
		},
		
		//显示或影藏category
		showChildCategory:function(child){
			if(this._storeCategorys[child.category] != 1){
				child.showCategory(false);
			}
			else{
				child.showCategory(true);
			}
			this._storeCategorys[child.category]--;
		},
		
		//腾出最大的domNode空间，使最上面的item可以碰到底部
		reOffsetChild:function(d){
			array.forEach(this.getChildren(),function(child){
				child.domNode.style.top = parseFloat(child.domNode.style.top) + parseFloat(d) + "px";
			});
			this.totalHeight(d);
		},
		
		//偏移child，使其靠的更近
		offsetChildDistance:function(child,idx){
			if(idx == 0){
				child.domNode.style.top = -this.childOffset - 40 +"px";
			}
			else{
				child.domNode.style.top = -(this.offset*idx+this.childOffset+40)+"px";
			}
		},
		
		//containerNode 高度
		totalHeight:function(d){
			if(!d){
				d = 0;
			}
			this.domNode.style.height = (this.getChildren().length-1)  * this.childOffset +parseFloat(d) +"px";;
		},
		
		resize:function(){
		//	this.inherited(arguments);
			var _this = this;
			array.forEach(this.getChildren(),function(child,idx){
				//显示或者隐藏category
				_this.showChildCategory(child);
				//偏移child，使其靠的更近
				_this.offsetChildDistance(child,idx);
				
			})
			//containerNode 高度
			this.totalHeight();
		},
	});
	return CardexContainer;
})