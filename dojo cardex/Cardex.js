﻿define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dojo/dom-construct",
	"dojo/_base/array",
	"dojo/_base/lang",
	"dojox/mobile/ScrollableView",
	"demo/_ScrollableMixin",
	"demo/CardexContainer",
	"dijit/_Container",
	"dijit/_contained",
],function(declare,WidgetBase,domConstruct,array,lang,ScrollableView,ScrollableMixin,CardexContainer,Container, Contained){
	var Cardex = declare("demo.CardexContainer",[WidgetBase,Container, Contained,ScrollableMixin],{
		//传给cardexContainer的store
		store:null,
		
		tag:"div",
		
		baseClass:"dijitCardex",
		
		scrollableParams: null,
		
		keepScrollPos: false,
		//最后可拖动的item
		dragItem:null,
		
		constructor: function(args){
			lang.mixin(this,args)
			this.scrollableParams = {noResize: true};
		},
		
		buildRendering:function(){
			this.domNode = this.srcNodeRef || domConstruct.create(this.tag);
			this.domNode.style.overflow = "hidden";
			this.domNode.style.position = "relative";
			this.domNode.style.top = "0px";
			
			this.titleNode = domConstruct.create("div",{className:"dijitCardexTitle"},this.domNode);
			//由于是3D状态，通过平面的touchNode将位置转移到立体的containerNode
			this.touchNode = domConstruct.create("div",{className:"dijitCardexTouchNode"},this.domNode);
			this.touchNode.style.overflow = "hidden";
			this.slopeNode = domConstruct.create("div",{className:"dijitCardexSlopeNode"},this.touchNode);
			this.containerNode = domConstruct.create("div",{className:"dijitCardexContainer"},this.slopeNode);
			this.inherited(arguments);
		},
		
		
		postCreate:function(){
			this.inherited(arguments);
			this.child = new CardexContainer({store:this.store});
			this.addChild(this.child);
		},
		
		startup: function(){
			this.childs = this.child.getChildren();
		//	if(this._started){ return; }
			this.inherited(arguments);
		},
		
		_setTitleAttr:function(text){
			this.title = text;
			this.titleNode.innerHTML = text;
		},
		
		
		//获取偏移的顶部点
		getSlope:function(){
			if(this.child.childOffset){
				this.top = -this.child.childOffset;
			}
			this.bottom = -(this.child.domNode.clientHeight - parseFloat(this.showSlope));
		},
		
		
		resize: function(){
			if(!this._i){
				this._i = 01;
			}
			else{
				return;
			}
			this.inherited(arguments);
			//重新排列item，腾出domNode空间
			this.child.reOffsetChild(this.showSlope);
			//获取偏移的顶部点
			this.getSlope();
			/*			
			array.forEach(this.getChildren(), function(child){
				if(child.resize){ child.resize(); }
			});
			*/	
		},
		
		setStore:function(){
			this.child.setStore(store, /*String*/query, /*Object*/queryOptions);
			//重新调整距离
			this.child.resize();
		},
		
		getDragItem:function(){
			console.log("draged");
			return this.dragItem;
		}
	})

	return Cardex;
})