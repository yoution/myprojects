﻿define([
	"dojo/_base/declare",
	"dojox/mobile/_StoreListMixin",
	"demo/CardexItem",
	"dojo/_base/lang"
],function(declare,StoreListMixin,CardexItem,lang){
	return declare([StoreListMixin],{
		//类别字段
	//	categoryField:"category",
		
		/*
		buildRendering: function(){
			//设置类别字段
			if(this.store.category){
				this.categoryField = this.store.category;
			}
			this.inherited(arguments);
		},
		*/
		createListItem: function(item){
			var props = {};
			if(!item["label"]){
				props["label"] = item[this.labelProperty];
			}
			/*
			//暂存category
			if(!this._showCategorys[item["category"]]){
				this._showCategorys[item["category"]] = 1;
			}
			else{
				this._showCategorys[item["category"]]++;
			}
			*/
			for(var name in item){
				props[(this.itemMap && this.itemMap[name]) || name] = item[name];
			}
			return new CardexItem(lang.mixin(props,{parent:this}));
		},
	})
})