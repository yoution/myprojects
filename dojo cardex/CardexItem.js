﻿define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dojo/dom-construct",
	"dojo/dom-geometry",
	"dojo/dom-class",
	"dojo/dom-style"
],function(declare,WidgetBase,domConstruct,domGeom,domClass,domStyle){
	var CardexItem = declare("demo.CardexItem",[WidgetBase],{
		//里面有许多文件
		baseClass:"dijitCardexItem",
		
		tag:"div",
		//是否显示标签
		_showCategory:false,
		
		//用相对定位时的偏移
		//_offsetDistance: 0,
		
		buildRendering:function(){
			this.domNode = this.srcNodeRef || domConstruct.create(this.tag);
			this.inherited(arguments);
			var categoryNodeContainer = domConstruct.create("div",{style:"height:40px"},this.domNode); 
			//创建顶部分类node
			this.categoryNode = domConstruct.create("div",{className:'dijitCardexCategory',innerHTML:""},categoryNodeContainer);
			//创建headNode
			this.headNode = domConstruct.create("div",{className:"dijitCardexItemTitle",innerHTML:'head'},this.domNode);
			//创建containerNode
			this.containerNode = domConstruct.create("div",{className:"dijitCardexItemContent",innerHTML:'content'},this.domNode);
		},
		
		startup:function(){ 
			if(this._started){ return; }
			this.inherited(arguments);
			if(!this._isOnLine){
				this._isOnLine = true;
				this.set("category",this.category);
			}
		},
		
		//显示或者关闭顶部的标签
		showCategory:function(show){
			if(show){
				this._showCategory = true;
				this.categoryNode.style.visibility = "visible";
			}
			else{
				this._showCategory = false;
				this.categoryNode.style.visibility = "hidden";
			}
		},
		
		_setCategoryAttr:function(category){
			this._set("category",category);
			this.categoryNode.innerHTML = category;
		},
		
		_setTitleAttr:function(text){
			this._set("title",text);
			this.headNode.innerHTML = text;
		},
		
		_setLabelAttr: function(text){
			this._set("label", text);
			this.containerNode.innerHTML = text;
		},
		
		onClick:function(){
		
		},
	})
	
	return CardexItem;
})