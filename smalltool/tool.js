a = (function(){

		//记录每次的状态,
		//[{},{}],{sta:xx,nodes:xx};
		//sta表示状态，nodes表示所有节点
		var status = [];

		//选择器操作
		//str为传入的选择字符窜
		//b：option(""，0,1)默认表示变红加边框
		//		0:影藏
		//		1:获取父节点
		function selector(str,b){
			var a = document.querySelectorAll(str),
					length = a.length,
					obj = {};
			
			if(!length){
				console.log("selected none");
				return;
			}
			console.log("  "+length +"  "+ "elements selected");

			//将每次的状态记录
			obj.sta = b;
			obj.nodes = a;
			status.push(obj);
			
			//进行选择操作
			while(a[--length]){
				if(b === 1)
					a[length].parentNode.style.border = "solid red 5px";
				else if(b === 0)
					a[length].style.display = "none";
				else
					a[length].style.border = "solid red 5px";
			}
			console.log("document.querySelectorAll(\""+str+"\")");
		}

		//取消操作
		function reback(){
			var length,lSta,lNodes;
			var statu = status.pop();

			if(!statu){
				console.log("initaled");
				return;
			}
			//还原上次的信息
			lNodes = statu.nodes;
			lSta = statu.sta
			length = lNodes.length;


			while(lNodes[--length]){
				if(lSta ===1 ){
					lNodes[length].parentNode.style.border = "";
				}
				else if(lSta === 0){
					lNodes[length].style.display = "block";
				}
				else{
					lNodes[length].style.border = "";
				}
			}

		}


		try{
			return{
				//str为传入的选择字符窜
				//b：option(""，0,1)默认表示变红加边框
				//		0:影藏
				//		1:获取父节点
				a:selector,
				//回复操作
				b:reback, 
			}
		}
		finally{
			var data = new Date();
			setTimeout(function(){
				console.log("!--groupbegin")
				console.log("!--website="+location.href);
				console.log("!--title="+document.title);
				console.log("!--lastmodify="+data.getFullYear() +"-" +(data.getMonth()+1) +"-"+data.getDate())
				console.log("\n");
				console.log("!--groupend");
			},300);
		}
})()

